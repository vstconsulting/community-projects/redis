# Quick Redis deployment

Project for quick Redis deployment with [Polemarch](https://github.com/vstconsulting/polemarch). 

Supported OS
------------
This project is able to deploy Redis on hosts with following OS:

* Ubuntu;
* Debian;
* RedHat;
* CentOS;
* Arch Linux.

Project's playbook
------------------

* **deploy.yml**: This playbook deploys Redis on all hosts from
selected inventory.

Enjoy it!